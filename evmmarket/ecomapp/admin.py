# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from ecomapp.models import Category, Brands, Product, CartItem, Cart, Order

# Register your models here.
admin.site.register(Category)
admin.site.register(Brands)
admin.site.register(Product)
admin.site.register(CartItem)
admin.site.register(Cart)
admin.site.register(Order)